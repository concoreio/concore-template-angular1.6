Essa é uma estrutura padronizada com tudo que você precisa para criar pequenas aplicações angular 1.6 com o concore.

Ele possui;
1. Webpack
2. Angular 1.6
3. Angular Material
4. Babel
5. Upload automático para S3
6. ESLint
7. Yarn

Para rodar o template;
yarn
npm run serve

Para buildar e fazer o upload no S3:
npm run build

Note que para que o build tenha sucesso é necessário criar um arquivo .env com as seguintes variáveis na raiz do seu projeto;

AWS_ACCESS_KEY_ID=[ SUA KEY_ID ]
AWS_SECRET_ACCESS_KEY=[ SUA ACESS_KEY ]
BUCKET=[ NOME DO BUCKET ]
REGION=[ REGIAO DO BUCKET ]

Sobre a estrutura do projeto;

Este template é componentizado e utiliza a sintaxe Javascript ES6. Dentro da pasta src:
- index.html -> HTML raiz da aplicação, sugerimos não alterar nada além do title da página.
- index.scss -> Sass raiz da aplicação, deve ser usada para importar estilos externos a aplicação.
-
- assets
  - images
- components -> Os componentes da sua aplicação
  - ConcoreBrand -> Componente para aplicar o logo do concore.
  - SocialButton -> Componente para aplicar botões de redes sociais.
  - Wait -> Componente para exibir em telas de "loading".
- modules -> módulos da aplicação
  - AuthModule -> Módulo pronto para realizar o login no concore.
  - TemplateModule -> Exemplo de módulo