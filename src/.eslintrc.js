module.exports = {
  extends: [
    'angular'
  ],
  rules: {
    'angular/no-service-method': 0,
    'angular/log': 0,
    'angular/window-service': 0,
    'angular/controller-as-route': 0,
    'angular/on-watch': 0,
    'max-params': ["error", 6]
  }
}
