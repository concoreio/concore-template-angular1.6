import './ConcoreBrand.scss';
import template from './ConcoreBrand.html';
import concoreForColored from './concore-for-colored.svg';
import concoreForWhite from './concore-for-white.svg';

class ConcoreBrandController {
  constructor() {
    this.backgroundTypes = ['colored', 'white'];
    this.sizeTypes = ['large', 'medium', 'small', 'xs'];
    this.background = 'white';
    this.size = 'medium';
  }

  $onChanges() {
    if (!this.backgroundTypes.includes(this.background)) {
      throw new Error(`[background] just permit options: ${this.backgroundTypes.join(', ')}`);
    }

    if (!this.sizeTypes.includes(this.size)) {
      throw new Error(`[size] just permit options: ${this.sizeTypes.join(', ')}`);
    }

    this.srcIcon = concoreForWhite;

    if (this.background === 'colored') {
      this.srcIcon = concoreForColored;
    }
  }
}

export const ConcoreBrand = {
  template,
  controller: ConcoreBrandController,
  bindings: {
    background: '@',
    size: '@'
  }
};
