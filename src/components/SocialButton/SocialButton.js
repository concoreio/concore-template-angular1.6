import './SocialButton.scss';
import template from './SocialButton.html';

class SocialButtonController {
  constructor() {
    this.socials = {
      facebook: {
        icon: 'fb'
      },
      google: {
        icon: 'google'
      },
      linkedin: {
        icon: 'in'
      }
    };

    this.transparent = false;
    this.type = 'button';
    this.social = undefined;
    this.withIcon = false;
  }

  $onChanges() {
    if (!this.social) {
      return false;
    }

    const socialsOptions = Object.keys(this.socials);

    if (!socialsOptions.includes(this.social)) {
      throw new Error(`Set the [social] option with options: ${socialsOptions.join(', ')}`);
    }

    let iconColor = 'white';
    let additionalClass = '';

    if (this.transparent) {
      iconColor = 'color';
      additionalClass = 'btn-transparent';
    }

    this.stylus = {
      className: `btn-${this.social} ${additionalClass}`,
      iconSrc: `/assets/images/icons/${this.socials[this.social].icon}-${iconColor}.svg`,
      withIcon: this.withIcon
    };
  }
}

export const SocialButton = {
  template,
  controller: SocialButtonController,
  transclude: true,
  bindings: {
    transparent: '@',
    type: '@',
    social: '@',
    withIcon: '@'
  }
};

