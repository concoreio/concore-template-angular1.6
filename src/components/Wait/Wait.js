import './Wait.scss';
import template from './Wait.html';

class WaitController { }

export const Wait = {
  template,
  controller: WaitController,
  controllerAs: 'vm',
  bindings: {
    action: '@'
  }
};
