import angular from 'angular';

import TemplateModule from './modules/TemplateModule/TemplateModule';
import AuthModule from './modules/AuthModule/AuthModule';

import './setup';

import './index.scss';

export const app = 'app';

angular
  .module(app, [
    AuthModule,
    TemplateModule
  ])
  ;
