import angular from 'angular';
import 'angular-animate';
import 'angular-aria';
import 'angular-material';
import 'angular-messages';
import 'angular-ui-router';

import ConcoreTheme from '../../theme.module';

import routesConfig from './AuthRoutes';

import {ForgotPasswordComponent, LoginComponent, RegisterComponent} from './components';

const AuthModule = 'auth';
angular
  .module(AuthModule, [
    'ngAnimate',
    'ngAria',
    'ngMaterial',
    'ngMessages',
    'ui.router',

    ConcoreTheme
  ])
  .config(routesConfig)

  .run(() => {
    'ngInject';
  })

  .component('forgotPassword', ForgotPasswordComponent)
  .component('login', LoginComponent)
  .component('register', RegisterComponent)
;

export default AuthModule;
