import Concore from 'concore-sdk-js';

export default routesConfig;

/** @ngInject */
function routesConfig($stateProvider, $urlRouterProvider, $locationProvider) {
  $locationProvider.html5Mode(true).hashPrefix('!');
  $urlRouterProvider.otherwise('/');

  $stateProvider
    .state('login', {
      url: '/login',
      component: 'login'
    })
    .state('logout', {
      url: '/logout',
      data: {requiredLogin: true},
      controller: ($rootScope, $state) => {
        window.localStorage.clear();
        Concore.Datacore.Auth.logout();
        $rootScope.$broadcast('auth:user:logout', null);
        $state.go('login');
      },
      template: 'Exiting...'
    })
    .state('register', {
      url: '/register',
      component: 'register'
    })
    .state('forgot-password', {
      url: '/forgot-password',
      component: 'forgotPassword'
    })
    .state('wait', {
      url: '/wait',
      component: 'wait'
    })
  ;
}
