import Concore from 'concore-sdk-js';

import ForgotPasswordTmpl from './ForgotPassword.html';
import LoginTmpl from './Login.html';
import RegisterTmpl from './Register.html';

class AuthController {
  constructor($rootScope, $window, $state, $scope) {
    'ngInject';

    this.$rootScope = $rootScope;
    this.routeMain = $rootScope.routeMain;
    this.$state = $state;
    this.$window = $window;
    this.$scope = $scope;
    this.MoleculeQuery = Concore.Datacore.MoleculeQuery;
    this.Auth = Concore.Datacore.Auth;
    this.organizations = [];
    this.isLogging = false;
    this.isLoadingOrg = false;
    this.user = {};
    this.error = null;
    this.success = null;

    Object.assign($rootScope, {
      rootClasses: [
        'body-auth',
        'body-wide'
      ].join(' ')
    });
  }

  shouldEnableLogin() {
    /* if (!this.organization) {
      return false;
    } */
    if (!this.user.email) {
      return false;
    }
    if (!this.user.password) {
      return false;
    }

    return true;
  }

  auth($event) {
    $event.preventDefault();
    this.error = null;
    this.isLogging = true;
    const {Auth} = Concore.Datacore;
    let user = Promise.resolve(Auth.getUser());

    if (!Auth.isLogged()) {
      user = Auth.login(this.user.email, this.user.password);
    }

    user
      .then(() => {
        this.$rootScope.$broadcast('auth:user:login', Auth.getUser());

        if (this.routeMain) {
          return this.$state.go(this.routeMain);
        }

        this.$window.location.href = '/';

        return false;
      })
      .catch(error => {
        if (error.message === 'User not found!') {
          error.message = 'Usuário não encontrado';
        }

        if (error.message === 'Invalid username/password.') {
          error.message = 'Senha incorreta';
        }

        this.error = error;
        this.isLogging = false;
        this.$scope.$apply();
      });
  }

  setOrg(orgName) {
    if (!orgName) {
      return Promise.reject(new Error('Organização não encontrada'));
    }

    if (orgName === 'master') {
      return Promise.resolve();
    }

    this.error = null;

    const query = new this.MoleculeQuery('Organizations');
    return query.equalTo('name', orgName).limit(1).find()
      .then(resp => {
        if (resp.results.length === 0) {
          throw new Error('Organização não encontrada');
        }

        const org = resp.results[0];
        this.initConcore(process.env.apiHost, org.getAtoms('appId'), org.getAtoms('apiKey'));
        return Promise.resolve(org);
      });
  }

  forgotPassword($event) {
    $event.preventDefault();
  }

  register($event) {
    $event.preventDefault();
  }

  initConcore(host, appId, apiKey) {
    const storage = window.localStorage;
    storage.setItem('host', host);
    storage.setItem('appId', appId);
    storage.setItem('apiKey', apiKey);

    Concore.init(storage.getItem('host'), storage.getItem('appId'), storage.getItem('apiKey'));
  }
}

export const ForgotPassword = {
  template: ForgotPasswordTmpl,
  controller: AuthController,
  controllerAs: 'vm'
};

export const Login = {
  template: LoginTmpl,
  controller: AuthController,
  controllerAs: 'vm'
};

export const Register = {
  template: RegisterTmpl,
  controller: AuthController,
  controllerAs: 'vm'
};
