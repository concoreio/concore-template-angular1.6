export {
  ForgotPassword as ForgotPasswordComponent,
  Login as LoginComponent,
  Register as RegisterComponent
} from './AuthComponent/Auth';
