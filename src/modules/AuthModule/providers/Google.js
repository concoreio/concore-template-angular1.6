import { User } from 'models';
import hello from 'hellojs';

let requestedPermissions;
let authResponse;

const provider = {
  authenticate(options) {
    hello(this.getAuthType())
      .login({
        scope: requestedPermissions
      })
      .then(auth => {
        if (!options.success) {
          return false;
        }

        authResponse = auth.authResponse;

        return hello(auth.network)
          .api('me');
      }, response => { // hellojs dont support catch
        if (options.error) {
          options.error(this, response);
        }
      })
      .then(user => {
        options.success(this, {
          id: user.id,
          access_token: authResponse.access_token,
          expiration_date: new Date((authResponse.expires * 1000) + (new Date()).getTime()).toJSON()
        });
      }, response => {
        if (options.error) {
          options.error(this, response);
        }
      })
    ;
  },

  deauthenticate() {
    this.restoreAuthentication(null);
  },

  getAuthType() {
    return 'google';
  },

  restoreAuthentication(authData) {
    return true || authData;
  }
};

const GoogleUtils = {
  init() {
    User._registerAuthenticationProvider(provider);
  },

  api(path) {
    return hello('google').api(path);
  },

  isLinked(user) {
    return user._isLinked('google');
  },

  logIn(permissions, options) {
    requestedPermissions = permissions || 'profile';
    return User._logInWith('google', options);
  },

  fill(user) {
    return new Promise((resolve, reject) => {
      this.api('me')
        .then(googleUser => user.set({
          avatar: googleUser.picture,
          email: googleUser.email,
          emailVerified: true,
          name: googleUser.name,
          username: googleUser.email
        }).save().then(resolve).catch(reject), reject);
    });
  },

  link(user, permissions, options) {
    requestedPermissions = permissions;
    return user._linkWith('google', options);
  },

  unlink(user, options) {
    return user._unlinkFrom('google', options);
  }
};

export default GoogleUtils;
