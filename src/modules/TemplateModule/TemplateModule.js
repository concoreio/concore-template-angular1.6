import angular from 'angular';
import 'angular-animate';
import 'angular-aria';
import 'angular-material';
import 'angular-messages';
import uiRouter from 'angular-ui-router';
import 'angular-ui-router/release/stateEvents';
import 'angular-real/dist/angular-real';
import Concore from 'concore-sdk-js';

import ConcoreTheme from '../../theme.module';
import routesConfig from './TemplateRoutes';

import {Container} from './containers/Container';

import {
  MainComponent
} from './components';

const TemplateModule = 'TemplateModule';

angular.module(TemplateModule, [
  'AngularReal',
  'ngAnimate',
  'ngAria',
  'ngMaterial',
  'ngMessages',
  uiRouter,
  'ui.router.state.events',
  ConcoreTheme
])
.config(routesConfig)
.component('container', Container)
.component('main', MainComponent)

.run(($rootScope, $state) => {
  'ngInject';

  $rootScope.$on('$stateChangeStart', (event, nextState) => {
    let requiredLogin = false;

    if (nextState.data && nextState.data.requiredLogin) {
      requiredLogin = true;
    }

    if (!requiredLogin && Concore.Datacore.Auth.isLogged()) {
      event.preventDefault();
      $state.go('template.main');
    } else if (requiredLogin && !Concore.Datacore.Auth.isLogged()) {
      event.preventDefault();
      $state.go('login');
    }
  });
})

.directive('autoFocus', $timeout => {
  'ngInject';

  return {
    restrict: 'AC',
    link: (_scope, _element) => {
      $timeout(() => {
        _element[0].focus();
      }, 500);
    }
  };
})
;

export default TemplateModule;
