export default routesConfig;

/** @ngInject */
function routesConfig($stateProvider, $urlRouterProvider, $locationProvider) {
  $locationProvider.html5Mode(true).hashPrefix('!');
  $urlRouterProvider.otherwise('/main');

  $stateProvider
  .state('template', {
    abstract: true,
    data: {requiredLogin: true},
    component: 'container'
  })
    .state('template.main', {
      url: '/main',
      component: 'main'
    });
}
