import Concore from 'concore-sdk-js';
import template from './Main.html';
import './Main.scss';

class MainController {
  constructor($state, $timeout) {
    'ngInject';

    this.$state = $state;
    this.$timeout = $timeout;
  }

  logout() {
    this.isLoading = true;
    this.action = 'Encerrando seção ...';

    this.$timeout(() => {
      this.$state.go('logout');
    }, 3000);
  }

  $onInit() {
    // Initialization code here...
    this.user = Concore.Datacore.Auth.getUser();
  }
}

export const MainComponent = {
  controller: MainController,
  template,
  controllerAs: 'vm'
};
