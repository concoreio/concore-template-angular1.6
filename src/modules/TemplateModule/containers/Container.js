// import {Datacore} from 'concore-sdk-js';

import template from './Container.html';
import './Container.scss';

class ContainerController {
  constructor($state, $window, $rootScope, $mdSidenav) {
    'ngInject';
    const _this = this;

    this.$state = $state;
    this.$window = $window;
    this.$mdSidenav = $mdSidenav;
    this.$rootScope = $rootScope;
   
    this.$rootScope.$on('general.pageTitle', () => {
      _this.pageTitle = _this.$rootScope.pageTitle;
    });
  }

  toogleSideNav() {
    this.$mdSidenav('left').toggle();
  }

  closeSideNav() {
    this.$mdSidenav('left').toggle();
  }
}

export const Container = {
  controller: ContainerController,
  template,
  controllerAs: 'vm'
};
