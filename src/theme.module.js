import angular from 'angular';
import 'angular-animate';
import 'angular-aria';
import 'angular-material';

import {ConcoreBrand, SocialButton, Wait} from './components';

const ConcoreTheme = 'concore.theme';

angular
  .module(ConcoreTheme, [
    'ngMaterial'
  ])
  .config($mdThemingProvider => {
    'ngInject';

    $mdThemingProvider.theme('default')
      .primaryPalette('blue')
      .accentPalette('indigo');

    $mdThemingProvider.theme('navigation')
      .primaryPalette('grey')
      .accentPalette('blue-grey')
      .dark();

    $mdThemingProvider.theme('dark')
      .primaryPalette('cyan')
      .accentPalette('blue-grey')
      .dark();

    $mdThemingProvider.theme('error')
      .primaryPalette('red')
      .accentPalette('red')
      .dark();
  })

  .component('concoreBrand', ConcoreBrand)
  .component('socialButton', SocialButton)
  .component('wait', Wait)
;

export default ConcoreTheme;
